package com.example.multiplechoice_app.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.multiplechoice_app.R;
import com.example.multiplechoice_app.apdater.AdapterMyFragment;
import com.example.multiplechoice_app.apdater.AdapterPhieuDapAn_Navigation;
import com.example.multiplechoice_app.common.APIUtills;
import com.example.multiplechoice_app.common.DataClient;
import com.example.multiplechoice_app.evenbus.EventBusType;
import com.example.multiplechoice_app.fragment.MyFragment;
import com.example.multiplechoice_app.model.JSONExamsObject;
import com.example.multiplechoice_app.model.JSONQuestionObject;
import com.example.multiplechoice_app.model.JSONExamLogObject;
import com.example.multiplechoice_app.model.ModelPhieuDapAn_Naviagation;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LamBaiActivity extends AppCompatActivity {
    private JSONExamsObject jsonExamsObject = null;
    private ArrayList<JSONQuestionObject> arrayQuestion;

    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.sliding_tabs)
    TabLayout tabLayout;
    ArrayList<MyFragment> fragmentList;
    AdapterMyFragment adapterMyFragment;

    @BindView(R.id.toolBarMain)
    Toolbar toolbar;
    @BindView(R.id.drawerlayout)
    DrawerLayout drawerLayout;
    @BindView(R.id.txt_lambai_made)
    TextView txt_lambai_made;
    @BindView(R.id.txt_cauhoihientai)
    TextView txt_cauhoihientai;

    private   int socauhoi;
    private   int exam_id;
    public static int student_id;
    public static JSONExamLogObject examLogObject;
    // tính thời gian
    @BindView(R.id.count_timeText)
    TextView count_timeText;
    private CountDownTimer countDownTimer;
    long timelambai = 0;
    int  phut = 0;
    int giay = 0;

    //Drawable Navigation
    static ArrayList<ModelPhieuDapAn_Naviagation>list_modelPhieuDapAn_Navigation;
    static AdapterPhieuDapAn_Navigation adapterPhieuDapAnNavigation;
    @BindView(R.id.lvnavigationView_MHC)
    ListView lvnavigationView_MHC;
    static TextView txt_hoanthanh;
    public static ArrayList<JSONExamLogObject.QuestionLog> arrayQueslog ;
    private  int soCauDung = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lam_bai);
        ButterKnife.bind(this);
            getDatafromIntent();
            Init();
    }

    private void getDatafromIntent() {
        Intent intent = getIntent();
        try {
            jsonExamsObject = new Gson().fromJson(intent.getStringExtra("JSONExamsObject"), JSONExamsObject.class);
            Type type = new TypeToken<ArrayList<JSONQuestionObject>>() {}.getType();
            arrayQuestion = new Gson().fromJson(intent.getStringExtra("JSON_QUESTION"), type);
            student_id = Integer.parseInt(intent.getStringExtra("STUDENT_ID"));
            Type type2 = new TypeToken<ArrayList<JSONExamLogObject.QuestionLog>>() {}.getType();
            arrayQueslog = new Gson().fromJson(intent.getStringExtra("JSON_QUESTION_LOG"), type2);
            socauhoi = arrayQuestion.size();
            exam_id = Integer.parseInt(jsonExamsObject.getExamId());
            Log.d("arrayQuestion_SIZE", arrayQuestion.size() +"");
            Log.d("jsonExamsObject_NAME", jsonExamsObject.getExamName()+"");
            Log.d("STUDENT_ID", student_id+"");
            Log.d("JSON_QUESTION_LOG", arrayQueslog.size()+"");
            Log.d("JSON_QUESTION_LOG", arrayQueslog.get(0).getCorrectAnswer()+"");
        }catch (Exception e){
            Log.d("LOI", e.toString().trim());
            socauhoi = 0;
            exam_id = 0;
        }

    }
    private void Init() {
        if(jsonExamsObject != null){
            try {
                txt_lambai_made.setText("Mã " + exam_id);
                timelambai= (long)(Integer.parseInt(jsonExamsObject.getNumberOfQuestions())*60000);
            }catch (Exception e){
                Toast.makeText(this, "Error Server", Toast.LENGTH_SHORT).show();
                Log.d("LOI",e.toString());
            }
        }
        txt_hoanthanh =findViewById(R.id.txt_hoanthanh);
        initArrayFragment();
        initCountDownTimer();
        initDrawerLayout();
        ActionBar();
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float positionOffset, int positionOffsetPixels) {
                txt_cauhoihientai.setText(i+1+"\\"+socauhoi);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }



    public void initArrayFragment(){
        fragmentList = new ArrayList<>();
        genFragmentList();
        adapterMyFragment = new AdapterMyFragment(getSupportFragmentManager(),this,fragmentList);
        viewPager.setAdapter(adapterMyFragment);
        tabLayout.setupWithViewPager(viewPager);
    }
    private void genFragmentList() {
        for(int  i = 0 ; i < socauhoi ; i ++){
            JSONQuestionObject jsonQuestionObject = arrayQuestion.get(i);
            JSONExamLogObject.QuestionLog questionLog = arrayQueslog.get(i);
            Log.d("json_QuestionLog11111", new Gson().toJson(questionLog));
            fragmentList.add((MyFragment) MyFragment.newInstance(i
                        ,new Gson().toJson(jsonQuestionObject)
                        ,new Gson().toJson(questionLog)));
        }
    }


    private void xulymoDialogNopBai() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(LamBaiActivity.this);
        builder.setTitle(R.string.nopbai);
        builder.setMessage(R.string.banchacchanmuonnopbai);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.huy, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.setNegativeButton(R.string.dongy, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Calendar calendar = Calendar.getInstance();
                String strDateFormat24 = "HH:mm";
                SimpleDateFormat sdf1 = new SimpleDateFormat(strDateFormat24);
                String time = sdf1.format(calendar.getTime());
                //get day
                String strDateFormat = "dd/MM/yyyy";
                SimpleDateFormat sdf2 = new SimpleDateFormat(strDateFormat);
                String day = sdf2.format(calendar.getTime());
                //get time đã làm bài
                long timedalambai = timelambai -(phut*60000+giay*1000);
                int phutdalam= (int) (timedalambai / 60000);
                int giaydalam  = (int) (timedalambai % 60000 / 1000);
                String s;
                s = phutdalam +":";
                if(giaydalam < 10){
                    s = s +"0"+giaydalam;
                }
                else {
                    s = s + giaydalam +"";
                }
                countDownTimer.onFinish();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
    public void xulymoDialogTinhDiem(){
        if(arrayQueslog.size() == socauhoi){
            for(int i = 0 ; i < socauhoi ; i ++){
                JSONExamLogObject.QuestionLog questionLog = arrayQueslog.get(i);
                Log.d("DAP_AN" ,questionLog.getChoosenAnswer() +"-" + questionLog.getCorrectAnswer());
                if(questionLog.getChoosenAnswer().equals(questionLog.getCorrectAnswer()))
                    soCauDung++;

            }
        }


        Dialog dialogTinhDiem;
        count_timeText.setText("Hết giờ !");
        countDownTimer.cancel();
        dialogTinhDiem = new Dialog(LamBaiActivity.this);
        dialogTinhDiem.requestWindowFeature(Window.FEATURE_NO_TITLE);// no title
        dialogTinhDiem.setContentView(R.layout.custom_dialog_tinhdiem);
        dialogTinhDiem.setCanceledOnTouchOutside(false); // click ra ngoài có tắt DiaLog không
        dialogTinhDiem.setCancelable(false);

        Button btn_dialoglamdemoi = dialogTinhDiem.findViewById(R.id.btn_dialoglamdemoi);
        Button btn_dialogxemketqua = dialogTinhDiem.findViewById(R.id.btn_dialogxemketqua);
        TextView txt_dialogsocaudung    = dialogTinhDiem.findViewById(R.id.txt_dialogsocaudung);
        TextView txt_dialogsodiem    = dialogTinhDiem.findViewById(R.id.txt_dialogsodiem);
        //txt_dialogsodiem.setText("Chưa có dữ liệu để tính điểm");
        txt_dialogsocaudung.setText(soCauDung+"/" + socauhoi);
        float d = (float)Math.round(((float)(soCauDung*10)/(float) socauhoi)*100)/100;
        if(soCauDung == 0)
            txt_dialogsodiem.setText(d +" điểm");
        else
            txt_dialogsodiem.setText(d +" điểm");

        examLogObject = new JSONExamLogObject();
        JSONExamLogObject.ExamLog examLog = new JSONExamLogObject.ExamLog();
        examLog.setExamId(exam_id);
        examLog.setTotalScore(d+"");
        examLog.setStudentId(student_id);
        examLog.setDateTime(getTime());
        examLogObject.setExamLog(examLog);
        examLogObject.setQuestionLog(arrayQueslog);
        //sendExamLog(new Gson().toJson(examLog), new Gson().toJson(arrayQueslog));
        sendExamLog(examLog, examLogObject);

        dialogTinhDiem.show();

        btn_dialoglamdemoi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogTinhDiem.cancel();
                startActivity(new Intent(LamBaiActivity.this,TrangChuActivity.class));
                LamBaiActivity.this.finish();

            }
        });
        btn_dialogxemketqua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LamBaiActivity.this, XemDapAnActivity.class);
                intent.putExtra("jsonExamsObject",new Gson().toJson(jsonExamsObject));
                intent.putExtra("arrayQuestion",new Gson().toJson(arrayQuestion));
                intent.putExtra("arrayQueslog",new Gson().toJson(arrayQueslog));
                intent.putExtra("socaudung",soCauDung);
                LamBaiActivity.this.startActivity(intent);
                LamBaiActivity.this.finish();
            }
        });
    }

    private void sendExamLog(JSONExamLogObject.ExamLog jsonExamLog, JSONExamLogObject examLogObject) {
        Log.d("JSON_EXAMLOG",new Gson().toJson(jsonExamLog));
        Log.d("JSON_QUESTIONLOG",new Gson().toJson(arrayQueslog));
        DataClient dataClient = APIUtills.getData();
        Log.d("AAAAA3",new Gson().toJson(examLogObject));
        Call<String> call3 = dataClient.sendExamQues_log(examLogObject);
        call3.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d("RESULT_examLogObject", response.body());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("LOI", t.getMessage());
            }
        });

    }

    public static void  UpdateAnswer_Naviagation (int cau , String dapanduocchon){

        ModelPhieuDapAn_Naviagation modelPhieuDapAnNaviagation = list_modelPhieuDapAn_Navigation.get(cau);

        switch (dapanduocchon){

            case "A":{
                modelPhieuDapAnNaviagation.setDapanA(true);
                modelPhieuDapAnNaviagation.setDapanB(false);
                modelPhieuDapAnNaviagation.setDapanC(false);
                modelPhieuDapAnNaviagation.setDapanD(false);
                break;
            }

            case "B":{
                modelPhieuDapAnNaviagation.setDapanB(true);
                modelPhieuDapAnNaviagation.setDapanA(false);
                modelPhieuDapAnNaviagation.setDapanC(false);
                modelPhieuDapAnNaviagation.setDapanD(false);
                break;
            }

            case "C":{
                modelPhieuDapAnNaviagation.setDapanC(true);
                modelPhieuDapAnNaviagation.setDapanA(false);
                modelPhieuDapAnNaviagation.setDapanB(false);
                modelPhieuDapAnNaviagation.setDapanD(false);
                break;
            }

            case "D":{
                modelPhieuDapAnNaviagation.setDapanD(true);
                modelPhieuDapAnNaviagation.setDapanA(false);
                modelPhieuDapAnNaviagation.setDapanB(false);
                modelPhieuDapAnNaviagation.setDapanC(false);
                break;
            }

        }
        int dem = 0;
        for(int  i = 0 ; i < list_modelPhieuDapAn_Navigation.size() ; i ++){
            if(list_modelPhieuDapAn_Navigation.get(i).isDapanA() ||
                    list_modelPhieuDapAn_Navigation.get(i).isDapanB() ||
                    list_modelPhieuDapAn_Navigation.get(i).isDapanC() ||
                    list_modelPhieuDapAn_Navigation.get(i).isDapanD()){
                    dem++;
            }
        }
        //txt_hoanthanh.setText(LamBaiActivity.this.getResources().getString(R.string.dalam)+" "+dem+"\\"+socauhoi);
        adapterPhieuDapAnNavigation.notifyDataSetChanged();
    }

    @Override // tạo menu
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater =getMenuInflater();
        inflater.inflate(R.menu.mnu_done,menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        xulymoDialogNopBai();
        return super.onOptionsItemSelected(item);
    }

    private void initCountDownTimer(){
        countDownTimer = new CountDownTimer(timelambai, 1000) {
            public void onTick(long millisUntilFinished) {
                String text;
                phut= (int) (millisUntilFinished / 60000);
                giay   = (int) (millisUntilFinished % 60000 / 1000);
                text = phut +":";
                if(giay < 10){
                    text = text +"0"+giay;
                }
                else {
                    text = text + giay +"";
                }
                count_timeText.setText(text);
            }

            public void onFinish() {
                xulymoDialogTinhDiem();
            }
        }.start();


    }
    private  void initDrawerLayout(){
        list_modelPhieuDapAn_Navigation = new ArrayList<>();
        for(int  i = 0 ; i < socauhoi ; i ++){//fake data Navigation
            ModelPhieuDapAn_Naviagation dapAn = new ModelPhieuDapAn_Naviagation(i,false,false,false,false);
            list_modelPhieuDapAn_Navigation.add(dapAn);
        }
        adapterPhieuDapAnNavigation = new AdapterPhieuDapAn_Navigation(this,R.layout.item_multiple_choice_navigation,list_modelPhieuDapAn_Navigation);
        lvnavigationView_MHC.setAdapter(adapterPhieuDapAnNavigation);
    }

    @OnClick(R.id.viewPager)
    void onClick(View view){
        switch (view.getId()){

        }
    }

    @SuppressLint("RestrictedApi")
    private void ActionBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(android.R.drawable.ic_menu_sort_by_size);
        // bắt sự kiện để mở drawerlayoutMHC
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });
    }
    @Override
    public void onBackPressed() {
        // đóng listView
        if((drawerLayout.isDrawerOpen(GravityCompat.START))){
            drawerLayout.closeDrawer(GravityCompat.START);
        }else {
            xulymoDialogNopBai();

        }
    }
    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }
    @Subscribe
    public void updateQuesLog(EventBusType eventBusType){
        if(eventBusType.getStateChange() == EventBusType.StateChange.UPDATE_QUES_LOG){

            JSONExamLogObject.QuestionLog questionLog =
                    new Gson().fromJson(eventBusType.getJson(), JSONExamLogObject.QuestionLog.class);
            arrayQueslog.set(eventBusType.getPosition(),questionLog);
            Log.d("EVENT_BUS",questionLog.getChoosenAnswer()+"");

        }

    }

    private String getTime(){
        Calendar calendar = Calendar.getInstance();
        String strDateFormat24 = "HH:mm:ss";
        SimpleDateFormat sdf1 = new SimpleDateFormat(strDateFormat24);
        String time = sdf1.format(calendar.getTime());
        String strDateFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf2 = new SimpleDateFormat(strDateFormat);
        String day = sdf2.format(calendar.getTime());
        return  time +" - " +day;
    }

}
