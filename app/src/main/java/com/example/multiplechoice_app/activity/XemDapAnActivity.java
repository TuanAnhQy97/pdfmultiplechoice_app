package com.example.multiplechoice_app.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.multiplechoice_app.R;
import com.example.multiplechoice_app.apdater.AdapterDapAnFragment;
import com.example.multiplechoice_app.fragment.DapAnFragment;
import com.example.multiplechoice_app.model.JSONExamLogObject;
import com.example.multiplechoice_app.model.JSONExamsObject;
import com.example.multiplechoice_app.model.JSONQuestionObject;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import butterknife.ButterKnife;

public class XemDapAnActivity extends AppCompatActivity {
    Toolbar toolBarChamDiem;
    ViewPager viewPagerChamDiem;
    TabLayout sliding_tabsChamDiem;
    ArrayList<DapAnFragment> fragmentListChamDiem;
    AdapterDapAnFragment adapterMyFragmentChamDiem;
    TextView txt_lambai_socauDungChamDiem;
    TextView txt_cauhoihientaiChamDiem;

    private JSONExamsObject jsonExamsObject;
    private ArrayList<JSONQuestionObject> arrayQuestion;
    private ArrayList<JSONExamLogObject.QuestionLog> arrayQuesLog;
    private int socaudung ;
    private int socauhoi ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xem_dap_an);
        ButterKnife.bind(this);
        getDataFromIntent();

        init();
        addEvents();
        ActionBar();


    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        try {
            jsonExamsObject = new Gson().fromJson(intent.getStringExtra("jsonExamsObject"), JSONExamsObject.class);
            Type type1 = new TypeToken<ArrayList<JSONQuestionObject>>() {}.getType();
            arrayQuestion = new Gson().fromJson(intent.getStringExtra("arrayQuestion"), type1);
            Type type2 = new TypeToken<ArrayList<JSONExamLogObject.QuestionLog>>() {}.getType();
            arrayQuesLog = new Gson().fromJson(intent.getStringExtra("arrayQueslog"), type2);
            socaudung = intent.getIntExtra("socaudung",0);
            socauhoi = arrayQuestion.size();
            Log.d("socauhoi", socauhoi+"");
            Log.d("arrayDapAN", arrayQuestion.size()+"");
            Log.d("arraySaveAnswer", arrayQuesLog.size()+"");
        }catch (Exception e){
            Log.d("LOI", e.toString());
        }
    }

    private void init() {
        txt_lambai_socauDungChamDiem = findViewById(R.id.txt_lambai_socauDungChamDiem);
        txt_cauhoihientaiChamDiem = findViewById(R.id.txt_cauhoihientaiChamDiem);
        toolBarChamDiem = findViewById(R.id.toolBarChamDiem);
        viewPagerChamDiem = findViewById(R.id.viewPagerChamDiem);
        sliding_tabsChamDiem = findViewById(R.id.sliding_tabsChamDiem);
        fragmentListChamDiem = new ArrayList<>();
        genFragmentList();
        adapterMyFragmentChamDiem = new AdapterDapAnFragment(getSupportFragmentManager(),this,fragmentListChamDiem);
        viewPagerChamDiem.setAdapter(adapterMyFragmentChamDiem);
        sliding_tabsChamDiem.setupWithViewPager(viewPagerChamDiem);
        txt_lambai_socauDungChamDiem.setText(getResources().getString(R.string.dung)+" " + socaudung+"/"+socauhoi);
    }
    private void addEvents() {
        viewPagerChamDiem.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
                txt_cauhoihientaiChamDiem.setText(i+1+"\\"+socauhoi);
            }

            @Override
            public void onPageSelected(int i) {

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }


    private void genFragmentList() {
        for(int  i = 0 ; i < socauhoi ; i ++){
            JSONQuestionObject jsonQuestionObject = arrayQuestion.get(i);
            JSONExamLogObject.QuestionLog questionLog = arrayQuesLog.get(i);
            fragmentListChamDiem.add((DapAnFragment) DapAnFragment.newInstance(i
                    ,new Gson().toJson(questionLog)
                    ,new Gson().toJson(jsonQuestionObject)));
        }
    }
    @Override // tạo menu
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater =getMenuInflater();
        inflater.inflate(R.menu.mnu_new,menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(XemDapAnActivity.this,TrangChuActivity.class));
        finish();
        return super.onOptionsItemSelected(item);
    }
    @SuppressLint("RestrictedApi")
    private void ActionBar() {
        setSupportActionBar(toolBarChamDiem);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

    }
}
