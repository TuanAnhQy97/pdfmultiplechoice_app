package com.example.multiplechoice_app.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.multiplechoice_app.R;
import com.example.multiplechoice_app.apdater.ExamAdapter;
import com.example.multiplechoice_app.common.APIUtills;
import com.example.multiplechoice_app.common.CheckConnectiom;
import com.example.multiplechoice_app.common.DataClient;

import com.example.multiplechoice_app.common.LinkURL;
import com.example.multiplechoice_app.listener.ItemCallback;
import com.example.multiplechoice_app.model.JSONExamLogObject;
import com.example.multiplechoice_app.model.JSONExamsObject;
import com.example.multiplechoice_app.model.JSONQuestionObject;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExamActivity extends AppCompatActivity {
    @BindView(R.id.rela_notconnected)
    RelativeLayout rela_notconnected;
    @BindView(R.id.rela_connected)
    RelativeLayout rela_connected;
    ArrayList<JSONExamsObject> arrayExam;
    ExamAdapter examAdapter;
    @BindView(R.id.recyclerViewDeThi)
    RecyclerView recyclerViewDeThi;
    @BindView(R.id.avi_loading)
    AVLoadingIndicatorView avi_loading;
    @BindView(R.id.rela_download)
    RelativeLayout rela_download;
    @BindView(R.id.car_download)
    CardView car_download;
    @BindView(R.id.rela_content)
    RelativeLayout rela_content;
    @BindView(R.id.txt_notData)
    TextView txt_notData;
    @BindView(R.id.txt_phantram)
    TextView txt_phantram;

    @BindDrawable(R.drawable.custom_line_lambai_chon)
    Drawable custom_line_lambai_chon;
    @BindDrawable(R.drawable.custom_line_lambai_khongchon)
    Drawable custom_line_lambai_khongchon;

    private  String subject_code = "";
    private  boolean checkConnect;
    private  ArrayList<JSONQuestionObject> arrayQues;
    private  int ques_current;
    private JSONExamsObject jsonExamsObject;
    private String student_id;
    private  DataClient dataClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_de_thi);
        ButterKnife.bind(this);
        checkConnect = CheckConnectiom.checkCon(ExamActivity.this);
        addControls();
    }


    private void addControls() {
        dataClient = APIUtills.getData();
        Intent  intent = getIntent();
        subject_code= intent.getStringExtra("SUBJECT_CODE");
        student_id = intent.getStringExtra("STUDENT_ID");
        Log.d("SUBJECT_CODE",subject_code+"");
        Log.d("STUDENT_ID",student_id+"");
        arrayExam = new ArrayList<>();
        arrayQues = new ArrayList<>();
        examAdapter = new ExamAdapter(arrayExam,this,itemCallback);
        recyclerViewDeThi.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewDeThi.setHasFixedSize(true);
        recyclerViewDeThi.setAdapter(examAdapter);
        checkConnect = CheckConnectiom.checkCon(ExamActivity.this);
        if(!checkConnect){
            rela_notconnected.setVisibility(View.VISIBLE);
            rela_connected.setVisibility(View.GONE);
            txt_notData.setVisibility(View.GONE);
        }else{
            rela_notconnected.setVisibility(View.GONE);
            rela_connected.setVisibility(View.VISIBLE);
            txt_notData.setVisibility(View.GONE);
            getExams();
        }


    }

    public void loadRecyclerDeThi(String response){
        Type type = new TypeToken<ArrayList<JSONExamsObject>>() {}.getType();
        arrayExam = new Gson().fromJson(response, type);
        if(arrayExam.size() > 0){
            examAdapter.setNewData(arrayExam);
            avi_loading.hide();
        }else {
            avi_loading.hide();
            rela_download.setVisibility(View.GONE);
            rela_content.setVisibility(View.GONE);
            txt_notData.setVisibility(View.VISIBLE);
        }
    }
    private void getExams(){
        avi_loading.show();
        StringRequest request = new StringRequest(Request.Method.POST, LinkURL.urlgetExams,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response != null){
                            loadRecyclerDeThi(response);
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("LOI", error.toString()+"");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String> params = new HashMap<>();
                params.put("subject_id",subject_code== null ? "0" : subject_code);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(request);
    }

    ItemCallback itemCallback = new ItemCallback() {
        @Override
        public void execute(int position) {
            ExamAdapter.ViewHolder viewHolder = (ExamAdapter.ViewHolder) recyclerViewDeThi.findViewHolderForAdapterPosition(position);
            viewHolder.line1.setBackground(custom_line_lambai_chon);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    viewHolder.line1.setBackground(custom_line_lambai_khongchon);
                }
            },100);
            jsonExamsObject = arrayExam.get(position);
            Animation anim = AnimationUtils.loadAnimation(ExamActivity.this,R.anim.translate_down_up_rela);
            rela_download.setVisibility(View.VISIBLE);
            car_download.setAnimation(anim);
            rela_content.setVisibility(View.GONE);
            deleteData_befor();
            getQuestons(jsonExamsObject.getExamId().trim());
        }
    };

    private static void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        boolean delete = fileOrDirectory.delete();
    }
    private void getQuestons(String exam_id) {
            Call<String> call_Question = dataClient.getquestions(exam_id);
            call_Question.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    if(response.body() != null && !response.body().isEmpty()){
                        Log.d("JSON_QUESTION_",response.body());
                        Type type = new TypeToken<ArrayList<JSONQuestionObject>>() {}.getType();
                        arrayQues = new Gson().fromJson(response.body(), type);
                        File file = new File(getFilesDir() ,"jsonExam/pdf");
                        if(!file.exists())  file.mkdirs();
                        if(arrayQues.size() > 0){
                            ques_current = 0;
                            for(int  i = 0 ; i < arrayQues.size() ; i ++)
                                downloadandWritePdfofQuestion(file.getPath(),i);
                        }else {
                            avi_loading.hide();
                            rela_download.setVisibility(View.GONE);
                            rela_content.setVisibility(View.GONE);
                            txt_notData.setVisibility(View.VISIBLE);
                        }
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.d("LOI",t.getMessage());
                }
            });

    }

    private void downloadandWritePdfofQuestion(String path_PDf, int i) {
            String question_id = arrayQues.get(i).getQuestionId().trim();
            String link_ques = arrayQues.get(i).getQuestionPath().trim();
            link_ques = link_ques.substring(1);// bỏ dau / , bị thua dau /
        File file1 = new File(path_PDf,"cau"+question_id + ".pdf");
        Call<ResponseBody> call = dataClient.downloadFileFDP(link_ques);
             if(!call.isCanceled()){
                 call.enqueue(new Callback<ResponseBody>() {
                     @Override
                     public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                         FileOutputStream out = null;
                         try {
                             out  = new FileOutputStream(file1);
                             out.write(response.body().bytes());
                             out.close();
                             ques_current++;Log.d("QUES_CURRENT",ques_current +" ");
                             txt_phantram.setText(ques_current*100/arrayQues.size() +"%" );
                             if(ques_current == arrayQues.size()){
                                 downLoaded();
                                 return;
                             }
                         } catch (FileNotFoundException e) {
                             e.printStackTrace();
                         } catch (IOException e) {
                             e.printStackTrace();
                         }

                     }
                     @Override
                     public void onFailure(Call<ResponseBody> call, Throwable t) {
                         Log.d("LOI1", t.getMessage());
                         downloadandWritePdfofQuestion(path_PDf,ques_current);
                     }
                 });
             }

    }

    public void downLoaded(){
        ArrayList<JSONExamLogObject.QuestionLog> arrayQueslog  = new ArrayList<>();
        for(int  i = 0 ; i < arrayQues.size() ; i ++){
            arrayQueslog.add(new JSONExamLogObject.QuestionLog(""
                    ,arrayQues.get(i).getCorrectAnswer()
                    ,Integer.parseInt(student_id),Integer.parseInt(arrayQues.get(i).getQuestionId())
                    ,""));
        }
        Intent intent = new Intent(ExamActivity.this,LamBaiActivity.class);
        intent.putExtra("JSON_QUESTION",new Gson().toJson(arrayQues));
        intent.putExtra("JSONExamsObject",new Gson().toJson(jsonExamsObject));
        intent.putExtra("STUDENT_ID",student_id);
        intent.putExtra("JSON_QUESTION_LOG",new Gson().toJson(arrayQueslog));
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
    @OnClick(R.id.btn_thulai)
    void onClick(View view){
        switch (view.getId()){
            case R.id.btn_thulai:
                checkConnect = CheckConnectiom.checkCon(ExamActivity.this);
                if(checkConnect == false){
                    rela_notconnected.setVisibility(View.VISIBLE);
                    rela_connected.setVisibility(View.GONE);
                }else{
                    rela_notconnected.setVisibility(View.GONE);
                    rela_connected.setVisibility(View.VISIBLE);
                    getExams();
                }
                break;
        }
    }

    private void deleteData_befor(){
        String path = getFilesDir()+"/jsonExam";
        File dir = new File(path);
        if (dir.exists()) {
            if (dir.isDirectory()) {
                for (File child : dir.listFiles()) {
                    deleteRecursive(child);
                }
            }
            boolean delete = dir.delete();
        }
    }
}
