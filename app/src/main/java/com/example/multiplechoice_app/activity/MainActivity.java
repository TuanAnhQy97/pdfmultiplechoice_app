package com.example.multiplechoice_app.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.multiplechoice_app.R;
import com.example.multiplechoice_app.common.APIUtills;
import com.example.multiplechoice_app.common.DataClient;
import com.example.multiplechoice_app.model.JSONStudentObject;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.btn_sign_in)
    Button btn_sign_in;
    static final int GOOGLE_SIGN = 123;
    private  FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;
    private JSONStudentObject studentObject;
    private  SharedPreferences pre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        getSharedPreferences();

    }

    @OnClick(R.id.btn_sign_in)
    void onClick(View v){
        switch (v.getId()){
            case R.id.btn_sign_in:
                signIn();
                break;
        }
    }

    void createRequest(){
        mAuth = FirebaseAuth.getInstance();
        GoogleSignInOptions gso = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }
    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, GOOGLE_SIGN);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == GOOGLE_SIGN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("AAAAAA", "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }

    }

    private void updateUI(GoogleSignInAccount account) {
        // GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getActivity());
        if (account != null) {
            String personName = account.getDisplayName();
            String personGivenName = account.getGivenName();
            String personFamilyName = account.getFamilyName();
            String personEmail = account.getEmail();
            String personId = account.getId();
            Uri personPhoto = account.getPhotoUrl();

            Log.d("BBBBB", personName +"");
            Log.d("BBBBB", personGivenName +"");
            Log.d("BBBBB", personFamilyName +"");
            Log.d("BBBBB", personEmail +"");
            Log.d("BBBBB", personId +"");
            Log.d("BBBBB", personPhoto +"");

            sendInforClient(personEmail,personName,personPhoto.toString());

        }

    }

    private void sendInforClient(String personEmail,String personName,String personPhoto) {
        DataClient dataClient = APIUtills.getData();
        Call<String> call = dataClient.requesrStudent(personName,personEmail);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.body() != null){
                    Log.d("RESULT",response.body());
                    try {
                        studentObject = new Gson().fromJson(response.body(),JSONStudentObject.class);
                        studentObject.setUrl_Photo(personPhoto);

                        pre=getSharedPreferences("Infor_lognin", MODE_PRIVATE);
                        SharedPreferences.Editor edit=pre.edit();
                        edit.putString("INFOR_STUDENT", new Gson().toJson(studentObject));
                        edit.commit();

                        Intent intent = new Intent(MainActivity.this,TrangChuActivity.class);
                        intent.putExtra("STUDENT",new Gson().toJson(studentObject));
                        startActivity(intent);
                        finish();
                    }catch (Exception e){
                        Log.d("LOI",e.toString());
                    }



                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("LOI",t.getMessage());
            }
        });
    }
    private void getSharedPreferences(){
        pre=getSharedPreferences("Infor_lognin", MODE_PRIVATE);
        String json = pre.getString("INFOR_STUDENT","");
        if(!json.isEmpty()){
            studentObject = new Gson().fromJson(json,JSONStudentObject.class);
            Intent intent = new Intent(MainActivity.this,TrangChuActivity.class);
            intent.putExtra("STUDENT",new Gson().toJson(studentObject));
            startActivity(intent);
            finish();
        }else
            createRequest();
    }
}
