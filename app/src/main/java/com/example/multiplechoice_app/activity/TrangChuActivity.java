package com.example.multiplechoice_app.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.example.multiplechoice_app.R;
import com.example.multiplechoice_app.apdater.ViewPagerAdapter;
import com.example.multiplechoice_app.common.CheckConnectiom;
import com.example.multiplechoice_app.fragment.AccountFragment;
import com.example.multiplechoice_app.fragment.HomeFragment;
import com.example.multiplechoice_app.model.JSONStudentObject;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.BaseMultiplePermissionsListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TrangChuActivity extends AppCompatActivity {
    @BindView(R.id.btn_thulai)
    Button btn_thulai;
    @BindView(R.id.rela_notconnected)
    RelativeLayout rela_notconnected;
    @BindView(R.id.rela_connected)
    FrameLayout rela_connected;
    @BindView(R.id.botton_nav)
    BottomNavigationView botton_nav;
    @BindView(R.id.mViewpager)
    ViewPager mViewpager;
    ViewPagerAdapter adapter;
    private  boolean checkConnect;
    private JSONStudentObject studentObject;
    private HomeFragment homeFragment;
    private AccountFragment accountFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trang_chu);
        ButterKnife.bind(this);
        checkConnect = CheckConnectiom.checkCon(TrangChuActivity.this);
        init();
        if(checkConnect == false){
            rela_notconnected.setVisibility(View.VISIBLE);
            rela_connected.setVisibility(View.GONE);
        }else {
            rela_notconnected.setVisibility(View.GONE);
            rela_connected.setVisibility(View.VISIBLE);
            addEvents();
        }
    }


    private void init() {
        Intent intent = getIntent();
        studentObject = new Gson().fromJson(intent.getStringExtra("STUDENT"),JSONStudentObject.class);
        if(studentObject == null){
            SharedPreferences pre=getSharedPreferences("Infor_lognin", MODE_PRIVATE);
            String json = pre.getString("INFOR_STUDENT","");
            studentObject = new Gson().fromJson(json,JSONStudentObject.class);
        }
        Log.d("JSON_STUDENT",new Gson().toJson(studentObject));
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        homeFragment = (HomeFragment) HomeFragment.newInstance(studentObject.getStudentId()+"");
        accountFragment = (AccountFragment) AccountFragment.newInstance(intent.getStringExtra("STUDENT"));
        adapter.addFragment(homeFragment,"Home");
        adapter.addFragment(accountFragment,"Account");


        mViewpager.setAdapter(adapter);
        mViewpager.setOffscreenPageLimit(2);
        mViewpager.setCurrentItem(0, false);

    }

    private void addEvents() {
        botton_nav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.nav_ic_home:
                        mViewpager.setCurrentItem(0, false);
                        break;
                    case R.id.nav_ic_account:
                        mViewpager.setCurrentItem(1, false);
                        break;
                }
                return true;
            }
        });
        botton_nav.setSelectedItemId(R.id.nav_ic_home);

    }

    @OnClick(R.id.btn_thulai)
    void onClick(View view){
        switch (view.getId()){
            case R.id.btn_thulai:
                checkConnect = CheckConnectiom.checkCon(TrangChuActivity.this);
                if(checkConnect == false){
                    rela_notconnected.setVisibility(View.VISIBLE);
                    rela_connected.setVisibility(View.GONE);
                }else {
                    rela_notconnected.setVisibility(View.GONE);
                    rela_connected.setVisibility(View.VISIBLE);
                    addEvents();
                }
                break;
        }
    }


}
