package com.example.multiplechoice_app.listener;

public interface ItemCallback {
    void execute(int position );
}
