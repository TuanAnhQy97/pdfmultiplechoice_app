package com.example.multiplechoice_app.common;

public class APIUtills {
    //http://multiple-choice.000webhostapp.com/multiplechoiceapp/getsubjects.php
    public static  final String baseURL ="http://multiple-choice.000webhostapp.com/multiplechoiceapp/";
    //  dấu  / ở cuối là lấy file ở folder gốc retrofit_sinhvien

    // nhập và gửi dữ liệu đi
    public static DataClient getData(){
        return  RetrofitClient.getClient(baseURL).create(DataClient.class);
    }
}
