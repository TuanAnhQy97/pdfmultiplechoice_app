package com.example.multiplechoice_app.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.multiplechoice_app.R;
import com.example.multiplechoice_app.activity.ExamActivity;
import com.example.multiplechoice_app.activity.MainActivity;
import com.example.multiplechoice_app.activity.TrangChuActivity;
import com.example.multiplechoice_app.common.APIUtills;
import com.example.multiplechoice_app.common.CheckConnectiom;
import com.example.multiplechoice_app.common.DataClient;
import com.example.multiplechoice_app.apdater.SubjectAdapter;
import com.example.multiplechoice_app.common.LinkURL;
import com.example.multiplechoice_app.dbflow.MyDataBase;
import com.example.multiplechoice_app.dbflow.TypeDataSave;
import com.example.multiplechoice_app.listener.ItemCallback;
import com.example.multiplechoice_app.model.JSONSubjectObject;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    @BindView(R.id.txt_notData)
    TextView txt_notData;
    @BindView(R.id.avi_loading)
    AVLoadingIndicatorView avi_loading;
    @BindView(R.id.recyclerView_subject)
    RecyclerView recyclerView_subject;
    private ArrayList<JSONSubjectObject> listSubject;
    private SubjectAdapter adapter;
    private String student_id;

    public static Fragment newInstance(String student_id){
        HomeFragment homeFragment = new HomeFragment();
        Bundle bundle = new Bundle();
        bundle.putString("STUDENT_ID", student_id);
        homeFragment.setArguments(bundle);
        return homeFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this,view);
        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        student_id = bundle.getString("STUDENT_ID");
        Log.d("STUDENT_ID",student_id+"");
        init();
    }

    private void init() {
        recyclerView_subject.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView_subject.setHasFixedSize(true);
        listSubject = new ArrayList<JSONSubjectObject>();
        adapter = new SubjectAdapter(getActivity(),listSubject,itemCallback);
        recyclerView_subject.setAdapter(adapter);
        getSubject();

    }
    ItemCallback itemCallback = new ItemCallback() {
        @Override
        public void execute(int position) {
            Intent intent = new Intent(getActivity(), ExamActivity.class);
            intent.putExtra("SUBJECT_CODE",listSubject.get(position).getSubjectId());
            intent.putExtra("STUDENT_ID",student_id);
            getActivity().startActivity(intent);
        }
    };

    private void getSubject(){
        avi_loading.show();
        StringRequest request = new StringRequest(Request.Method.GET, LinkURL.urlgetSubjects,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            loadRecyView(response);
                        } else {
                            Log.d("JSONDETHI", "NULL NULL");
                            avi_loading.hide();
                            txt_notData.setVisibility(View.VISIBLE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("LOI", error.toString()+"");
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request);
    }

    private void loadRecyView(String response){
        Type type = new TypeToken<ArrayList<JSONSubjectObject>>(){}.getType();
        if(!response.isEmpty()){
            listSubject = new Gson().fromJson(response,type);
            adapter.setNewList(listSubject);
            avi_loading.hide();
        }
        else{
            avi_loading.hide();
        }
    }

    public String readDBFlowFav(){
        String s = MyDataBase.loadDataType(LinkURL.JSONSubject_Object);
        Log.d("JSON_LOAD", s);
        return s;
    }
    public  void saveDBFlowFav(String response){
        TypeDataSave typeDataSave = new TypeDataSave(LinkURL.JSONSubject_Object,response);
        MyDataBase.saveDataType(typeDataSave);
        Log.d("saveDBFlow", "saveDBFlow");

    }
}
