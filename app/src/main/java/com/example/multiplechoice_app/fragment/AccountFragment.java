package com.example.multiplechoice_app.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.multiplechoice_app.R;
import com.example.multiplechoice_app.activity.MainActivity;
import com.example.multiplechoice_app.model.JSONStudentObject;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class AccountFragment extends Fragment {

    @BindView(R.id.profile_image)
    CircleImageView profile_image;
    @BindView(R.id.txt_personName)
    TextView txt_personName;
    @BindView(R.id.btn_logout)
    CardView btn_logout;
    private  View view;
    private  GoogleSignInClient mGoogleSignInClient;
    private  FirebaseAuth mAuth;
    private JSONStudentObject jsonStudent;

    public static Fragment newInstance(String jsonStudent){
        AccountFragment accountFragment = new AccountFragment();
        Bundle bundle = new Bundle();
        bundle.putString("STUDENT", jsonStudent);
        accountFragment.setArguments(bundle);
        return accountFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_account, container, false);
        ButterKnife.bind(this,view);
        createRequest();
        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(jsonStudent == null){
            SharedPreferences pre=getActivity().getSharedPreferences("Infor_lognin", getActivity().MODE_PRIVATE);
            String json = pre.getString("INFOR_STUDENT","");
            jsonStudent = new Gson().fromJson(json,JSONStudentObject.class);
        }
        if(jsonStudent != null){
           // jsonStudent = new Gson().fromJson(bundle.getString("STUDENT"),JSONStudentObject.class);
            Log.d("JSON_STUDENT_URLPHOTO",jsonStudent.getUrl_Photo()+"");
            Picasso.with(getActivity()).load(jsonStudent.getUrl_Photo()).into(profile_image);
            txt_personName.setText(jsonStudent.getName());
            events();
        }
    }

    private void events() {
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGoogleSignInClient.signOut().addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        SharedPreferences pre=getActivity().getSharedPreferences("Infor_lognin", getActivity().MODE_PRIVATE);
                        SharedPreferences.Editor editor=pre.edit();
                        editor.clear();
                        editor.commit();
                        startActivity(new Intent(getActivity(), MainActivity.class));
                    }
                });
            }
        });
    }

    void createRequest(){
        mAuth = FirebaseAuth.getInstance();
        GoogleSignInOptions gso = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);
    }
}
