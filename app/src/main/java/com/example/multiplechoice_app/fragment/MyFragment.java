package com.example.multiplechoice_app.fragment;

import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.multiplechoice_app.R;
import com.example.multiplechoice_app.activity.LamBaiActivity;
import com.example.multiplechoice_app.common.LinkURL;
import com.example.multiplechoice_app.evenbus.EventBusType;
import com.example.multiplechoice_app.model.JSONExamLogObject;
import com.example.multiplechoice_app.model.JSONQuestionObject;
import com.github.barteksc.pdfviewer.PDFView;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyFragment extends Fragment {

    private  View view;
    private int page_now;
    private JSONQuestionObject jsonQuestionObject = null;
    private JSONExamLogObject.QuestionLog questionLog  = null;
    private String path_PDF;

    @BindView(R.id.pdf_viewer)
    PDFView pdfView;

    LinearLayout linear_choiceA,linear_choiceB,linear_choiceC,linear_choiceD;
    TextView txt_IdchoiceA,txt_IdchoiceB,txt_IdchoiceC,txt_IdchoiceD;
    //
    int id_Linear_now = -1 ; // đay là id của Linearlayout để lưu xem đáp án nào được chọn  ,
    // và nếu người dùng thay đổi đáp án thì id_Linear_now sẽ được cập nhậy


    public static Fragment newInstance(int page, String jsonQue,String jsonQueLog) {
        MyFragment fragmentFirst = new MyFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("page_current", page);
        bundle.putString("json_Question", jsonQue);
        bundle.putString("json_QuestionLog", jsonQueLog);
        fragmentFirst.setArguments(bundle);
        return fragmentFirst;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_my, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        page_now = bundle.getInt("page_current",0);
        // nếu setText page thì nó bắt đầu từ 0 còn nếu Toast thì nó bắt đầu từ 1
        jsonQuestionObject = new Gson().fromJson(bundle.getString("json_Question"),JSONQuestionObject.class);
        questionLog = new Gson().fromJson(bundle.getString("json_QuestionLog"), JSONExamLogObject.QuestionLog.class);
        Log.d("JSONQuestionObject", new Gson().toJson(jsonQuestionObject));
        Log.d("json_QuestionLog", new Gson().toJson(questionLog));
        Log.d("LINK_PDF", LinkURL.ip+jsonQuestionObject.getQuestionPath());
        initCompoment();
        addEvents();
    }

    private void addEvents() {
        if(!questionLog.getChoosenAnswer().isEmpty()){
            // neu cau tra loi dc tra loi
            if(questionLog.getChoosenAnswer().equals("A"))
                txt_IdchoiceA.setBackgroundResource(R.drawable.custom_circle_button_chon);
            else if(questionLog.getChoosenAnswer().equals("B"))
                txt_IdchoiceB.setBackgroundResource(R.drawable.custom_circle_button_chon);
            else if(questionLog.getChoosenAnswer().equals("C"))
                txt_IdchoiceC.setBackgroundResource(R.drawable.custom_circle_button_chon);
            else
                txt_IdchoiceD.setBackgroundResource(R.drawable.custom_circle_button_chon);
        }


        linear_choiceA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                questionLog.setChoosenAnswer("A");
                questionLog.setDateTime(getTime());
                sendEvensBusUpdateChoice();
                xulycautraloi(linear_choiceA.getId());
            }
        });
        linear_choiceB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                questionLog.setChoosenAnswer("B");
                questionLog.setDateTime(getTime());
                sendEvensBusUpdateChoice();
                xulycautraloi(linear_choiceB.getId());
            }
        });
        linear_choiceC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                questionLog.setChoosenAnswer("C");
                questionLog.setDateTime(getTime());
                sendEvensBusUpdateChoice();
                xulycautraloi(linear_choiceC.getId());
            }
        });
        linear_choiceD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                questionLog.setChoosenAnswer("D");
                questionLog.setDateTime(getTime());
                sendEvensBusUpdateChoice();
                xulycautraloi(linear_choiceD.getId());
            }
        });

    }


    private void initCompoment() {
        path_PDF  = getActivity().getFilesDir()+"/jsonExam/pdf/" + "cau"+jsonQuestionObject.getQuestionId() + ".pdf";
        File file = new File(path_PDF);
        if(file.exists()){
            pdfView.fromFile(file)
                    .password(null)
                    .invalidPageColor(Color.WHITE)
                    .load();
        }

        linear_choiceA = view.findViewById(R.id.linear_choiceA);
        linear_choiceB = view.findViewById(R.id.linear_choiceB);
        linear_choiceC = view.findViewById(R.id.linear_choiceC);
        linear_choiceD = view.findViewById(R.id.linear_choiceD);
        txt_IdchoiceA  = view.findViewById(R.id.txt_IdchoiceA);
        txt_IdchoiceB  = view.findViewById(R.id.txt_IdchoiceB);
        txt_IdchoiceC  = view.findViewById(R.id.txt_IdchoiceC);
        txt_IdchoiceD  = view.findViewById(R.id.txt_IdchoiceD);


    }


    private void xulycautraloi(int id_linearLayout) {

        if(linear_choiceA.getId() == id_linearLayout){
            txt_IdchoiceA.setBackgroundResource(R.drawable.custom_circle_button_chon);
            if(id_Linear_now != -1 && id_Linear_now != id_linearLayout){
                if(id_Linear_now == linear_choiceB.getId())
                    txt_IdchoiceB.setBackgroundResource(R.drawable.custom_circle_button_khong_chon);
                else if(id_Linear_now == linear_choiceC.getId())
                    txt_IdchoiceC.setBackgroundResource(R.drawable.custom_circle_button_khong_chon);
                if(id_Linear_now == linear_choiceD.getId())
                    txt_IdchoiceD.setBackgroundResource(R.drawable.custom_circle_button_khong_chon);
            }
        }
        else if(linear_choiceB.getId() == id_linearLayout){
            txt_IdchoiceB.setBackgroundResource(R.drawable.custom_circle_button_chon);
            if(id_Linear_now != -1 && id_Linear_now != id_linearLayout){
                if(id_Linear_now == linear_choiceA.getId())
                    txt_IdchoiceA.setBackgroundResource(R.drawable.custom_circle_button_khong_chon);
                else if(id_Linear_now == linear_choiceC.getId())
                    txt_IdchoiceC.setBackgroundResource(R.drawable.custom_circle_button_khong_chon);
                if(id_Linear_now == linear_choiceD.getId())
                    txt_IdchoiceD.setBackgroundResource(R.drawable.custom_circle_button_khong_chon);
            }
        }
        else if(linear_choiceC.getId() == id_linearLayout){
            txt_IdchoiceC.setBackgroundResource(R.drawable.custom_circle_button_chon);
            if(id_Linear_now != -1 && id_Linear_now != id_linearLayout){
                if(id_Linear_now == linear_choiceB.getId())
                    txt_IdchoiceB.setBackgroundResource(R.drawable.custom_circle_button_khong_chon);
                else if(id_Linear_now == linear_choiceA.getId())
                    txt_IdchoiceA.setBackgroundResource(R.drawable.custom_circle_button_khong_chon);
                if(id_Linear_now == linear_choiceD.getId())
                    txt_IdchoiceD.setBackgroundResource(R.drawable.custom_circle_button_khong_chon);
            }
        }
        else{
            txt_IdchoiceD.setBackgroundResource(R.drawable.custom_circle_button_chon);
            if(id_Linear_now != -1 && id_Linear_now != id_linearLayout){
                if(id_Linear_now == linear_choiceB.getId())
                    txt_IdchoiceB.setBackgroundResource(R.drawable.custom_circle_button_khong_chon);
                else if(id_Linear_now == linear_choiceC.getId())
                    txt_IdchoiceC.setBackgroundResource(R.drawable.custom_circle_button_khong_chon);
                if(id_Linear_now == linear_choiceA.getId())
                    txt_IdchoiceA.setBackgroundResource(R.drawable.custom_circle_button_khong_chon);
            }
        }

        id_Linear_now = id_linearLayout;
        //update Navigation
        LamBaiActivity.UpdateAnswer_Naviagation(page_now,questionLog.getChoosenAnswer());
    }

    private void sendEvensBusUpdateChoice(){
        EventBusType eventBusType = new EventBusType(
                EventBusType.StateChange.UPDATE_QUES_LOG,new Gson().toJson(questionLog),page_now);
        EventBus.getDefault().post(eventBusType);
    }
    private String getTime(){
        Calendar calendar = Calendar.getInstance();
        String strDateFormat24 = "HH:mm:ss";
        SimpleDateFormat sdf1 = new SimpleDateFormat(strDateFormat24);
        String time = sdf1.format(calendar.getTime());
        String strDateFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf2 = new SimpleDateFormat(strDateFormat);
        String day = sdf2.format(calendar.getTime());
        return  time +" - " +day;
    }
}
