package com.example.multiplechoice_app.fragment;

import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.multiplechoice_app.R;
import com.example.multiplechoice_app.model.JSONExamLogObject;
import com.example.multiplechoice_app.model.JSONQuestionObject;
import com.github.barteksc.pdfviewer.PDFView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class DapAnFragment extends Fragment {
    View view;
    @BindView(R.id.pdf_viewer)
    PDFView pdfView;
    @BindView(R.id.linear_choiceADapAn)
    LinearLayout linear_choiceADapAn;
    @BindView(R.id.txt_IdchoiceADapAn)
    TextView txt_IdchoiceADapAn;
    @BindView(R.id.linear_choiceBDapAn)
    LinearLayout linear_choiceBDapAn;
    @BindView(R.id.txt_IdchoiceBDapAn)
    TextView txt_IdchoiceBDapAn;
    @BindView(R.id.linear_choiceCDapAn)
    LinearLayout linear_choiceCDapAn;
    @BindView(R.id.txt_IdchoiceCDapAn)
    TextView txt_IdchoiceCDapAn;
    @BindView(R.id.linear_choiceDDapAn)
    LinearLayout linear_choiceDDapAn;
    @BindView(R.id.txt_IdchoiceDDapAn)
    TextView txt_IdchoiceDDapAn;
    @BindView(R.id.img_truefalseDapAn)
    ImageView img_truefalseDapAn;

    private int page_current;
    private  JSONQuestionObject jsonQuestionObject;
    private String path_PDF;
    private JSONExamLogObject.QuestionLog questionLog;


    public static Fragment newInstance(int page_current, String jsonQuesLog,String jsonQues) {
        DapAnFragment fragmentFirst = new DapAnFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("page_current", page_current);
        bundle.putString("jsonQuestionLog", jsonQuesLog);
        bundle.putString("jsonQuestionObject", jsonQues);
        fragmentFirst.setArguments(bundle);
        return fragmentFirst;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_dap_an, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        try {
            page_current = bundle.getInt("page_current",0);
            questionLog = new Gson().fromJson(bundle.getString("jsonQuestionLog"), JSONExamLogObject.QuestionLog.class);
            jsonQuestionObject = new Gson().fromJson(bundle.getString("jsonQuestionObject"),JSONQuestionObject.class);
            Log.d("jsonQuestionLog", bundle.getString("jsonQuestionLog"));
            Log.d("jsonQuestionObject", bundle.getString("jsonQuestionObject"));
            Log.d("arrayDapAN", bundle.getString("jsonQuestionObject")+"");
        }catch (Exception e){
            Log.d("LOI", e.toString());
        }
        Init();
    }

    private void Init() {
        path_PDF  = getActivity().getFilesDir()+"/jsonExam/pdf/" + "cau"+jsonQuestionObject.getQuestionId() + ".pdf";
        File file = new File(path_PDF);
        if(file.exists()){
            pdfView.fromFile(file)
                    .password(null)
                    .invalidPageColor(Color.WHITE)
                    .load();
        }
        initDapAn();
    }

    private void initDapAn() {

            if(questionLog != null){
                //int yourChoice = modelSaveAnswer.getNumber_answer();
                String yourChoice = questionLog.getChoosenAnswer();
                String severChoice = questionLog.getCorrectAnswer();
                //Toast.makeText(getContext(), yourChoice+"  = " + severChoice, Toast.LENGTH_SHORT).show();
                if(yourChoice.equals(severChoice)){
                    // neu cẩu trả lời trùng với đáp án của server
                    if(yourChoice.equals("A"))
                        txt_IdchoiceADapAn.setBackgroundResource(R.drawable.custom_button_true);
                    else if(yourChoice.equals("B"))
                        txt_IdchoiceBDapAn.setBackgroundResource(R.drawable.custom_button_true);
                    else if(yourChoice.equals("C"))
                        txt_IdchoiceCDapAn.setBackgroundResource(R.drawable.custom_button_true);
                    else
                        txt_IdchoiceDDapAn.setBackgroundResource(R.drawable.custom_button_true);

                    img_truefalseDapAn.setImageResource(R.drawable.ic_true_24dp);
                }
                else {
                    // nếu trả lời sai
                    if(yourChoice.isEmpty() || yourChoice  == null){
                        // nếu người thi cố tình không chọn thì ta hiện đáp án đúng nên
                        if(severChoice.equals("A"))
                            txt_IdchoiceADapAn.setBackgroundResource(R.drawable.custom_button_true);
                        else if(severChoice.equals("B"))
                            txt_IdchoiceBDapAn.setBackgroundResource(R.drawable.custom_button_true);
                        else if(severChoice.equals("C"))
                            txt_IdchoiceCDapAn.setBackgroundResource(R.drawable.custom_button_true);
                        else
                            txt_IdchoiceDDapAn.setBackgroundResource(R.drawable.custom_button_true);

                    }
                    else {
                        if(severChoice.equals("A"))
                            txt_IdchoiceADapAn.setBackgroundResource(R.drawable.custom_button_true);
                        else if(severChoice.equals("B"))
                            txt_IdchoiceBDapAn.setBackgroundResource(R.drawable.custom_button_true);
                        else if(severChoice.equals("C"))
                            txt_IdchoiceCDapAn.setBackgroundResource(R.drawable.custom_button_true);
                        else
                            txt_IdchoiceDDapAn.setBackgroundResource(R.drawable.custom_button_true);


                        if(yourChoice.equals("A"))
                            txt_IdchoiceADapAn.setBackgroundResource(R.drawable.custom_button_false);
                        else if(yourChoice.equals("B"))
                            txt_IdchoiceBDapAn.setBackgroundResource(R.drawable.custom_button_false);
                        else if(yourChoice.equals("C"))
                            txt_IdchoiceCDapAn.setBackgroundResource(R.drawable.custom_button_false);
                        else
                            txt_IdchoiceDDapAn.setBackgroundResource(R.drawable.custom_button_false);
                    }
                    img_truefalseDapAn.setImageResource(R.drawable.ic_false_24dp);
                }
        }
    }



}
