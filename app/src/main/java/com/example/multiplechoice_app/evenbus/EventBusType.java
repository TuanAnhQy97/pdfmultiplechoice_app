package com.example.multiplechoice_app.evenbus;

public class EventBusType {
    private StateChange stateChange;
    private String json;
    private int position;

    public EventBusType(StateChange stateChange, String json, int position) {
        this.stateChange = stateChange;
        this.json = json;
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public StateChange getStateChange() {
        return stateChange;
    }

    public void setStateChange(StateChange stateChange) {
        this.stateChange = stateChange;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public enum StateChange{
        UPDATE_QUES_LOG
    }
}
