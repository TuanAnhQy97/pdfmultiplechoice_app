package com.example.multiplechoice_app.apdater;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.multiplechoice_app.R;
import com.example.multiplechoice_app.activity.ExamActivity;
import com.example.multiplechoice_app.common.LinkURL;
import com.example.multiplechoice_app.listener.ItemCallback;
import com.example.multiplechoice_app.model.JSONSubjectObject;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubjectAdapter extends RecyclerView.Adapter<SubjectAdapter.MyViewHolder> {
       private Activity context;
        private ArrayList<JSONSubjectObject> listSubbject;
        private ItemCallback itemCallback;

    public SubjectAdapter(Activity context, ArrayList<JSONSubjectObject> listSubbject,ItemCallback itemCallback) {
        this.context = context;
        this.listSubbject = listSubbject;
        this.itemCallback = itemCallback;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_subject,parent,false);
        MyViewHolder viewHolder = new MyViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final JSONSubjectObject subject = listSubbject.get(position);
        holder.txt_namesubject.setText(subject.getSubjectName());
        Picasso.with(context).load(LinkURL.ip+subject.getIconUrl().trim()).error(R.drawable.ic_error24dp).into(holder.img_subject);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemCallback.execute(position);

            }
        });
    }

    @Override
    public int getItemCount() {
        return listSubbject != null ? listSubbject.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.img_subject)
        ImageView img_subject;
        @BindView(R.id.txt_namesubject)
        TextView txt_namesubject;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            Animation animation = AnimationUtils.loadAnimation(context,R.anim.translate_up_down);
            itemView.startAnimation(animation);
        }
    }
    public void setNewList(ArrayList<JSONSubjectObject> listSubject){
        this.listSubbject = listSubject;
        notifyDataSetChanged();
    }
}
