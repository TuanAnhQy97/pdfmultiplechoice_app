package com.example.multiplechoice_app.apdater;

import android.app.Activity;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.multiplechoice_app.R;
import com.example.multiplechoice_app.fragment.DapAnFragment;
import com.example.multiplechoice_app.fragment.MyFragment;

import java.util.ArrayList;

public class AdapterDapAnFragment extends FragmentPagerAdapter {

    Activity context;
    ArrayList<DapAnFragment> fragmentList;

    public AdapterDapAnFragment(FragmentManager fm, Activity context, ArrayList<DapAnFragment> fragmentList) {
        super(fm);
        this.context = context;
        this.fragmentList = fragmentList;
    }

    public AdapterDapAnFragment(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {

        String câu  = context.getResources().getString(R.string.cau)+" " + (position+1);
        return new StringBuilder(context.getResources().getString(R.string.cau)+" ").append(position + 1).toString();
    }
}
