package com.example.multiplechoice_app.apdater;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;


import com.example.multiplechoice_app.R;
import com.example.multiplechoice_app.activity.LamBaiActivity;
import com.example.multiplechoice_app.listener.ItemCallback;
import com.example.multiplechoice_app.model.JSONExamsObject;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ExamAdapter extends RecyclerView.Adapter<ExamAdapter.ViewHolder>  {

    private ArrayList<JSONExamsObject> arrayDeThi;
    private Activity contect;
    private ItemCallback itemCallback;


    public ExamAdapter(ArrayList<JSONExamsObject> jsonExamsObjects, Activity contect,ItemCallback itemCallback) {
        this.arrayDeThi = jsonExamsObjects;
        this.contect = contect;
        this.itemCallback = itemCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dethi,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        JSONExamsObject modelDeThi = arrayDeThi.get(i);
        viewHolder.txtnameDeThi.setText(modelDeThi.getExamName()+"");
        viewHolder.txt_socauhoiDeThi.setText(modelDeThi.getNumberOfQuestions()+" câu");
        viewHolder.txt_timelambaiDeThi.setText(modelDeThi.getTime()+" phút");
        viewHolder.btn_lambaiThi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    itemCallback.execute(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return  arrayDeThi == null ? 0 : arrayDeThi.size();
    }

    public class  ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.txt_nameDeThi)
        TextView txtnameDeThi;
        @BindView(R.id.txtmaDeThi)
        TextView txtmaDeThi;
        @BindView(R.id.txt_timelambaiDeThi)
        TextView txt_timelambaiDeThi;
        @BindView(R.id.btn_lambaiThi)
        Button btn_lambaiThi;
        @BindView(R.id.txt_socauhoiDeThi)
        TextView txt_socauhoiDeThi;
        @BindView(R.id.line1)
        public LinearLayout line1;

        ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            Animation animation = AnimationUtils.loadAnimation(contect,R.anim.translate_up_down);
            itemView.startAnimation(animation);
        }
    }

    private  void xulymoDialg(final JSONExamsObject modelDeThi ){
        AlertDialog.Builder builder = new AlertDialog.Builder(contect);
        builder.setTitle(R.string.lamdethi);
        builder.setMessage(R.string.bandasansanglamde);
        builder.setCancelable(false);

        builder.setPositiveButton(R.string.huy, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.setNegativeButton("Đồng Ý", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent(contect, LamBaiActivity.class);
                    intent.putExtra("JSONExamsObject",new Gson().toJson(modelDeThi));
                    contect.startActivity(intent);
                    contect.finish();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void setNewData(ArrayList<JSONExamsObject> list){
        this.arrayDeThi = list;
        notifyDataSetChanged();
    }


}
